﻿using Microsoft.EntityFrameworkCore;

namespace EjemploEFCodeFirstNC.Models
{
    public class PeliculasDBContext: DbContext
    {

        public PeliculasDBContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Peliculas> Peliculas { get; set; }

    }
}
