﻿using System.ComponentModel.DataAnnotations;

namespace EjemploEFCodeFirstNC.Models
{
    public class Peliculas
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string Titulo { get; set; }
        public DateTime FechaSalida { get; set; }
        [Required]
        [MaxLength(100)]
        public string Genero { get; set; }
        public decimal Taquilla { get; set; }
    }
}
